import { PageContainer } from 'components';
import { FC } from 'react';
import styled from 'styled-components';

interface Props {
  className?: string;
}

const Products: FC<Props> = ({ className }) => {
  return <PageContainer className={className}>Prorudcts</PageContainer>;
};

const StyledProducts = styled(Products)``;

export default StyledProducts;
