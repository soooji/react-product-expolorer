import { FC } from 'react';
import styled from 'styled-components';

interface Props {
  className?: string;
}

const Footer: FC<Props> = (props) => {
  return (
    <footer className={props.className}>
      Made with <span className="heart">♡</span> for PicNic
    </footer>
  );
};

const StyledFooter = styled(Footer)`
  width: 100%;
  height: 50px;

  display: flex;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;

  border-top: 1px solid ${({ theme }) => theme.colors.grey[1]};

  text-align: center;
  color: ${({ theme }) => theme.colors.grey[3]};

  span.heart {
    color: ${({ theme }) => theme.colors.error.main};
    padding: 0 ${({ theme }) => theme.space(1)};

    transform: scale(1.2);
  }
`;

export default StyledFooter;
