import { FC } from 'react';
import styled from 'styled-components';
import { Flex, Icon } from 'components';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { useNavigate } from 'react-router-dom';

interface Props {
  className?: string;
  showBack?: boolean;
}

const Header: FC<Props> = ({ className, showBack = false }) => {
  const navigate = useNavigate();

  const goBack = () => {
    navigate(-1);
  };

  return (
    <header className={className}>
      <Flex jc="space-between" ai="center" className="header-container">
        <div>
          {showBack ? (
            <button onClick={goBack} className="action --start">
              <Icon icon={faChevronLeft} />
            </button>
          ) : null}
        </div>
        <h2 className="main-logo">
          <span className="main-logo__normal" data-testid="HeaderLogo">
            Search for Products
          </span>
          <span className="main-logo__small">SfP</span>
        </h2>
        <div></div>
      </Flex>
    </header>
  );
};

const StyledHeader = styled(Header)`
  width: 100%;
  height: 60px;

  padding: 0 ${({ theme }) => theme.space(1)};

  background: ${({ theme }) => theme.colors.grey[1]};
  border-bottom: 1px solid ${({ theme }) => theme.colors.grey[2]};
  .header-container {
    width: 100%;
    height: 100%;
    max-width: 600px;

    margin: 0 auto;
    > div {
      width: 25%;
      height: 100%;
    }
    .main-logo {
      width: 50%;

      font-size: 1.4rem;
      font-weight: 800;
      letter-spacing: 2px;
      text-align: center;
      color: ${({ theme }) => theme.colors.primary.main};

      .main-logo__normal {
        display: initial;
      }
      .main-logo__small {
        display: none;
      }

      @media only screen and (max-width: 400px) {
        .main-logo__normal {
          display: none;
        }
        .main-logo__small {
          display: initial;
        }
      }
    }
  }

  button.action {
    color: white;
    font-size: 1.2rem;

    width: 100%;
    height: 100%;

    background: transparent;
    border: none;

    cursor: pointer;

    display: flex;
    align-items: center;

    padding: 0 ${({ theme }) => theme.space(4)};

    &.--end {
      justify-content: flex-end;
    }
    &.--start {
      justify-content: flex-start;
    }
  }
`;

export default StyledHeader;
