import clsx from 'clsx';
import { FC } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

import { Header, Footer } from './components';

interface Props {
  className?: string;
  containerClassName?: string;
  showBack?: boolean;
  pageTitle?: string;
}

const PageContainer: FC<Props> = ({
  className,
  containerClassName,
  showBack = false,
  children,
  pageTitle
}) => {
  return (
    <div className={className}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>{`Gif🤘Nic${pageTitle ? ` > ${pageTitle}` : ''}`}</title>
      </Helmet>
      <Header className="main-header" showBack={showBack} />
      <div className={clsx('page-content', containerClassName)}>{children}</div>
      <Footer className="footer-part" />
    </div>
  );
};

const StyledPageContainer = styled(PageContainer)`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  overflow-x: hidden;

  > .main-header {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;

    z-index: 111;
  }
  > .page-content {
    border-radius: ${({ theme }) => theme.borderRadius.normal};

    width: 100%;
    max-width: 1200px;

    padding: ${({ theme }) => theme.space(2)};
    margin: ${({ theme }) => theme.space(7)} auto 0 auto;
  }

  .footer-part {
    margin-top: auto;
  }
`;

export default StyledPageContainer;
