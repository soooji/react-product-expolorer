import { FC, StrictMode, Suspense } from 'react';

// CSS Imports
import 'assets/css/normalize/_normalize.scss';

// Theming
import { ThemeProvider } from 'styled-components';
import { theme, GlobalStyle } from 'assets/theme';

// Routes
import AppRoutes from 'routes';

const App: FC = () => {
  return (
    <StrictMode>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Suspense fallback={<p>Loading...</p>}>
          <AppRoutes />
        </Suspense>
      </ThemeProvider>
    </StrictMode>
  );
};

export default App;
