/*
  Global Styles which we want apply to all pages
  through style-component `createGlobalStyle`
*/

import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  /* Base Styles */
  body {
     background-color: #eee;
     margin: 0;
  }

  a {
    text-decoration: none;
    color: initial;
  }

  * {
    box-sizing: border-box;
    font-family: 'Comfortaa', cursive;

    /* width */
    ::-webkit-scrollbar {
      width: 7px;
      height: 7px;
      border-radius: 20px;
    }
    /* Track */
    ::-webkit-scrollbar-track {
      border-radius: 20px;
      background: rgba(0,0,0,.1);
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border-radius: 20px;
      background: rgba(0,0,0,.15);
    }
    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: rgba(0,0,0,.4);
    }
  }

  .noselect {
    -webkit-touch-callout: none; /* iOS Safari */
      -webkit-user-select: none; /* Safari */
      -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Old versions of Firefox */
          -ms-user-select: none; /* Internet Explorer/Edge */
              user-select: none; /* Non-prefixed version, currently
                                    supported by Chrome, Edge, Opera and Firefox */
  }

  .loading-skeleton {
    background: 
      linear-gradient(0.25turn, transparent, #fff, transparent),
      linear-gradient(#eee, #eee),
      radial-gradient(38px circle at 19px 19px, #eee 50%, transparent 51%),
      linear-gradient(#eee, #eee);  
    background-repeat: no-repeat;
    background-size: 315px 250px, 315px 180px, 100px 100px, 225px 30px; 
    background-position: -315px 0, 0 0, 0px 190px, 50px 195px; 
    animation: sekeltonLoading 0.5s infinite;
  }

  @keyframes sekeltonLoading {  
    to {
      background-position: 315px 0, 0 0, 0 190px, 50px 195px;
    }
  }
`;

export default GlobalStyle;
